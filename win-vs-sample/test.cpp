#include "pch.h"
#include "calc.h"

TEST(Calculation, alwaysTrue) {
	EXPECT_EQ(1, 1);
	EXPECT_TRUE(true);
}

TEST(Calculation, plus) {
	Calc calc = Calc();
	EXPECT_EQ(5, calc.calc_plus(2, 3));
}

TEST(TestCaseName, TestName) {
  EXPECT_EQ(1, 1);
  EXPECT_TRUE(true);
}

TEST(TestCaseName, TestFail) {
	EXPECT_EQ(1, 3);
}